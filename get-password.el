#!/usr/bin/emacs --script

(require 'auth-source)
(let ((query-result (car (auth-source-netrc-parse :file "~/.authinfo.gpg"
                                                  :host (elt argv 0)
                                                  :user (elt argv 0)
                                                  :port "imaps"
                                                  :max 1))))
  (setq result (alist-get "password" query-result nil nil #'equal)))
(princ (concat result "\n"))
